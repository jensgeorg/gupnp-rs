use crate::ffi::GSSDPResourceBrowser;
use gio::prelude::Cast;
use glib::object::IsA;
use glib::signal::connect_raw;
use glib::translate::*;
use glib::*;
use std::boxed::Box as Box_;

use std::mem::transmute;

use crate::ResourceBrowser;

pub trait ResourceBrowserExtManual: 'static {
    #[doc(alias = "resource-available")]
    fn connect_resource_available<F: Fn(&Self, &str, Vec<String>) + 'static>(
        &self,
        f: F,
    ) -> SignalHandlerId;
}

impl<O: IsA<ResourceBrowser>> ResourceBrowserExtManual for O {
    fn connect_resource_available<F: Fn(&Self, &str, Vec<String>) + 'static>(
        &self,
        f: F,
    ) -> SignalHandlerId {
        unsafe extern "C" fn resource_available_trampoline<
            P: IsA<ResourceBrowser>,
            F: Fn(&P, &str, Vec<String>) + 'static,
        >(
            this: *mut GSSDPResourceBrowser,
            usn: *mut libc::c_char,
            locations: *const glib::ffi::GList,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(
                ResourceBrowser::from_glib_borrow(this).unsafe_cast_ref(),
                &glib::GString::from_glib_borrow(usn),
                FromGlibPtrArrayContainerAsVec::from_glib_none_as_vec(locations),
            )
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"resource-available\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    resource_available_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}
