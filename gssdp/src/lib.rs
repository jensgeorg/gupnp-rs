pub use ffi;
pub use gio;
pub use glib;

/// No-op.
macro_rules! skip_assert_initialized {
    () => {};
}

macro_rules! assert_initialized_main_thread {
    () => {};
}

#[allow(unused_imports)]
mod auto;
pub use auto::*;

pub mod prelude;

mod resource_browser;
