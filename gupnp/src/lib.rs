pub use ffi;

/// No-op.
macro_rules! skip_assert_initialized {
    () => {};
}

/// No-op.
macro_rules! assert_initialized_main_thread {
    () => {};
}

pub use auto::*;
pub use gssdp;

mod auto;
pub mod prelude;
mod service_proxy;
mod state_variable_info;
pub use service_proxy::*;
