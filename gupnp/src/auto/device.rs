// This file was generated by gir (https://github.com/gtk-rs/gir)
// from ..
// from ../gir-files
// DO NOT EDIT

use crate::{ffi, DeviceInfo, RootDevice};
use glib::{prelude::*, translate::*};

glib::wrapper! {
    #[doc(alias = "GUPnPDevice")]
    pub struct Device(Object<ffi::GUPnPDevice, ffi::GUPnPDeviceClass>) @extends DeviceInfo;

    match fn {
        type_ => || ffi::gupnp_device_get_type(),
    }
}

impl Device {
    pub const NONE: Option<&'static Device> = None;
}

mod sealed {
    pub trait Sealed {}
    impl<T: super::IsA<super::Device>> Sealed for T {}
}

pub trait DeviceExt: IsA<Device> + sealed::Sealed + 'static {
    #[doc(alias = "root-device")]
    fn root_device(&self) -> Option<RootDevice> {
        ObjectExt::property(self.as_ref(), "root-device")
    }
}

impl<O: IsA<Device>> DeviceExt for O {}
