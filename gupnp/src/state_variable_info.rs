use glib::{
    translate::{from_glib, FromGlibPtrArrayContainerAsVec, UnsafeFrom},
    Type, Value,
};

use crate::auto::ServiceStateVariableInfo;

impl ServiceStateVariableInfo {
    pub fn name<'a>(&self) -> &'a str {
        unsafe {
            std::ffi::CStr::from_ptr((*self.as_ptr()).name)
                .to_str()
                .unwrap()
        }
    }

    pub fn send_events(&self) -> bool {
        unsafe { (*self.as_ptr()).send_events != 0 }
    }

    pub fn is_numeric(&self) -> bool {
        unsafe { (*self.as_ptr()).is_numeric != 0 }
    }

    pub fn type_(&self) -> Type {
        unsafe { from_glib((*self.as_ptr()).type_) }
    }

    pub fn default_value(&self) -> Value {
        unsafe { Value::unsafe_from((*self.as_ptr()).default_value) }
    }

    pub fn minimum(&self) -> Value {
        unsafe { Value::unsafe_from((*self.as_ptr()).minimum) }
    }

    pub fn maximum(&self) -> Value {
        unsafe { Value::unsafe_from((*self.as_ptr()).maximum) }
    }

    pub fn step(&self) -> Value {
        unsafe { Value::unsafe_from((*self.as_ptr()).step) }
    }

    pub fn allowed_values(&self) -> Vec<Value> {
        unsafe {
            FromGlibPtrArrayContainerAsVec::from_glib_none_as_vec((*self.as_ptr()).allowed_values)
        }
    }
}
