use crate::ServiceProxy;
use glib::object::IsA;
use glib::translate::{from_glib, from_glib_borrow, Borrowed, IntoGlib, ToGlibPtr};
use std::boxed::Box as Box_;

pub struct NotifyHandle {
    func: ffi::GUPnPServiceProxyNotifyCallback,
    user_data: glib::ffi::gpointer,
}

pub trait ServiceProxyExtManual: IsA<ServiceProxy> + 'static {
    #[doc(alias = "gupnp_service_proxy_add_notify")]
    fn add_notify<P: FnOnce(&ServiceProxy, &str, &glib::Value) + 'static>(
        &self,
        variable: &str,
        type_: glib::types::Type,
        callback: P,
    ) -> Option<NotifyHandle>;

    #[doc(alias = "gupnp_service_proxy_add_notify_full")]
    fn add_notify_full<P: Fn(&ServiceProxy, &str, &glib::Value) + 'static>(
        &self,
        variable: &str,
        type_: glib::types::Type,
        callback: P,
    ) -> Option<NotifyHandle>;

    #[doc(alias = "gupnp_service_proxy_add_raw_notify")]
    fn add_raw_notify<P: Fn(&ServiceProxy, &str, &glib::Value) + 'static>(
        &self,
        callback: P,
    ) -> Option<NotifyHandle>;

    #[doc(alias = "gupnp_service_proxy_remove_notify")]
    fn remove_notify<P: FnMut(&ServiceProxy, &str, &glib::Value)>(
        &self,
        variable: &str,
        handle: &NotifyHandle,
    ) -> bool;

    #[doc(alias = "gupnp_service_proxy_remove_raw_notify")]
    fn remove_raw_notify<P: FnMut(&ServiceProxy, &str, &glib::Value)>(
        &self,
        handle: &NotifyHandle,
    ) -> bool;
}

impl<O: IsA<ServiceProxy>> ServiceProxyExtManual for O {
    #[doc(alias = "gupnp_service_proxy_add_notify")]
    fn add_notify<P: FnOnce(&ServiceProxy, &str, &glib::Value) + 'static>(
        &self,
        variable: &str,
        type_: glib::types::Type,
        callback: P,
    ) -> Option<NotifyHandle> {
        let callback_data: Box_<P> = Box_::new(callback);
        unsafe extern "C" fn callback_func<
            P: FnOnce(&ServiceProxy, &str, &glib::Value) + 'static,
        >(
            proxy: *mut ffi::GUPnPServiceProxy,
            variable: *const libc::c_char,
            value: *mut glib::gobject_ffi::GValue,
            user_data: glib::ffi::gpointer,
        ) {
            let proxy = from_glib_borrow(proxy);
            let variable: Borrowed<glib::GString> = from_glib_borrow(variable);
            let value = from_glib_borrow(value);
            let callback: Box_<P> = Box_::from_raw(user_data as *mut _);
            (*callback)(&proxy, variable.as_str(), &value)
        }
        let super_callback0: Box_<P> = callback_data;
        unsafe {
            let handle = NotifyHandle {
                func: Some(callback_func::<P> as _),
                user_data: Box_::into_raw(super_callback0) as *mut _,
            };
            let result = from_glib(ffi::gupnp_service_proxy_add_notify(
                self.as_ref().to_glib_none().0,
                variable.to_glib_none().0,
                type_.into_glib(),
                handle.func,
                handle.user_data,
            ));
            if result {
                Some(handle)
            } else {
                None
            }
        }
    }

    #[doc(alias = "gupnp_service_proxy_add_notify_full")]
    fn add_notify_full<P: Fn(&ServiceProxy, &str, &glib::Value) + 'static>(
        &self,
        variable: &str,
        type_: glib::types::Type,
        callback: P,
    ) -> Option<NotifyHandle> {
        let callback_data: Box_<P> = Box_::new(callback);
        unsafe extern "C" fn callback_func<P: Fn(&ServiceProxy, &str, &glib::Value) + 'static>(
            proxy: *mut ffi::GUPnPServiceProxy,
            variable: *const libc::c_char,
            value: *mut glib::gobject_ffi::GValue,
            user_data: glib::ffi::gpointer,
        ) {
            let proxy = from_glib_borrow(proxy);
            let variable: Borrowed<glib::GString> = from_glib_borrow(variable);
            let value = from_glib_borrow(value);
            let callback: &P = &*(user_data as *mut _);
            (*callback)(&proxy, variable.as_str(), &value)
        }
        unsafe extern "C" fn notify_func<P: Fn(&ServiceProxy, &str, &glib::Value) + 'static>(
            data: glib::ffi::gpointer,
        ) {
            let _callback: Box_<P> = Box_::from_raw(data as *mut _);
        }
        let destroy_call5 = Some(notify_func::<P> as _);
        let super_callback0: Box_<P> = callback_data;
        unsafe {
            let handle = NotifyHandle {
                func: Some(callback_func::<P> as _),
                user_data: Box_::into_raw(super_callback0) as *mut _,
            };
            let result = from_glib(ffi::gupnp_service_proxy_add_notify_full(
                self.as_ref().to_glib_none().0,
                variable.to_glib_none().0,
                type_.into_glib(),
                handle.func,
                handle.user_data,
                destroy_call5,
            ));
            if result {
                Some(handle)
            } else {
                None
            }
        }
    }

    #[doc(alias = "gupnp_service_proxy_add_raw_notify")]
    fn add_raw_notify<P: Fn(&ServiceProxy, &str, &glib::Value) + 'static>(
        &self,
        callback: P,
    ) -> Option<NotifyHandle> {
        let callback_data: Box_<P> = Box_::new(callback);
        unsafe extern "C" fn callback_func<P: Fn(&ServiceProxy, &str, &glib::Value) + 'static>(
            proxy: *mut ffi::GUPnPServiceProxy,
            variable: *const libc::c_char,
            value: *mut glib::gobject_ffi::GValue,
            user_data: glib::ffi::gpointer,
        ) {
            let proxy = from_glib_borrow(proxy);
            let variable: Borrowed<glib::GString> = from_glib_borrow(variable);
            let value = from_glib_borrow(value);
            let callback: &P = &*(user_data as *mut _);
            (*callback)(&proxy, variable.as_str(), &value)
        }
        unsafe extern "C" fn notify_func<P: Fn(&ServiceProxy, &str, &glib::Value) + 'static>(
            data: glib::ffi::gpointer,
        ) {
            let _callback: Box_<P> = Box_::from_raw(data as *mut _);
        }
        let destroy_call3 = Some(notify_func::<P> as _);
        let super_callback0: Box_<P> = callback_data;
        unsafe {
            let handle = NotifyHandle {
                func: Some(callback_func::<P> as _),
                user_data: Box_::into_raw(super_callback0) as *mut _,
            };
            let result = from_glib(ffi::gupnp_service_proxy_add_raw_notify(
                self.as_ref().to_glib_none().0,
                handle.func,
                handle.user_data,
                destroy_call3,
            ));
            if result {
                Some(handle)
            } else {
                None
            }
        }
    }

    #[doc(alias = "gupnp_service_proxy_remove_notify")]
    fn remove_notify<P: FnMut(&ServiceProxy, &str, &glib::Value)>(
        &self,
        variable: &str,
        handle: &NotifyHandle,
    ) -> bool {
        unsafe {
            from_glib(ffi::gupnp_service_proxy_remove_notify(
                self.as_ref().to_glib_none().0,
                variable.to_glib_none().0,
                handle.func,
                handle.user_data,
            ))
        }
    }

    #[doc(alias = "gupnp_service_proxy_remove_raw_notify")]
    fn remove_raw_notify<P: FnMut(&ServiceProxy, &str, &glib::Value)>(
        &self,
        handle: &NotifyHandle,
    ) -> bool {
        unsafe {
            from_glib(ffi::gupnp_service_proxy_remove_raw_notify(
                self.as_ref().to_glib_none().0,
                handle.func,
                handle.user_data,
            ))
        }
    }
}
